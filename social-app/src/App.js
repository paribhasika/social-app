import React, { Component } from 'react';
import './App.css';
import {BrowserRouter as Router} from 'react-router-dom';

// Import all of our components
import Header from './containers/Header';
import Main from './Main/main';

class App extends Component {
  render() {
    return (
      
      <Router>
        <div className='wrapper'>
         <Header/>
          <Main/>
        </div>
      </Router>
    );
  }
}

export default App;
