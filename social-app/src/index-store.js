import { applyMiddleware, createStore ,compose} from 'redux'  
import createSagaMiddleware from 'redux-saga'  
import IndexReducer from './index-reducer'  
import IndexSagas from './index-saga'


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  IndexReducer,
  composeEnhancers(
  applyMiddleware(sagaMiddleware))
)

// Begin our Index Saga
sagaMiddleware.run(IndexSagas)
export default store;