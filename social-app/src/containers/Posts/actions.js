export const getPostData = (like,likes,comments,index)=>{
    return {
        type: 'POST_REQ',
        like,
        likes,
        comments,
        index
      }
}
export const likePost = (like,id,token,postId,index)=>{
    return {
        type: 'LIKE_POST',
        like,
        id,
        token,
        postId,
        index
      }
}
export const deletePost = (postId,id,token,index)=>{
    return {
        type: 'DELETE_POST',
        postId,id,token,index
      }
}
export const updateComments = (comments)=>{
    return {
        type: 'UPDATE_COMMENTS',
        comments
      }
}
