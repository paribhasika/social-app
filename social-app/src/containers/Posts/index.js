import React, {Component} from 'react'
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card'
import Typography from 'material-ui/Typography'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import DeleteIcon from 'material-ui-icons/Delete'
import FavoriteIcon from 'material-ui-icons/Favorite'
import FavoriteBorderIcon from 'material-ui-icons/FavoriteBorder'
import CommentIcon from 'material-ui-icons/Comment'
import Divider from 'material-ui/Divider'
import PropTypes from 'prop-types'
import {withStyles} from 'material-ui/styles'
import {Link} from 'react-router-dom'
import * as actions from './actions'
import { connect }  from 'react-redux'; 
import Comments from '../Comments'
import profilePic from '../../assets/profile-pic.png'


class Post extends Component {
  
  componentDidMount (){
    const {post,feed}=this.props;
    this.props.getPostData(this.checkLike(post.likes),post.likes.length,post.comments,this.props.index)
  }
  componentWillReceiveProps(props){
    this.commentList=props.post||this.props.post;
  }
  checkLike = (likes) => {
      const {user}=this.props
      let match = likes.indexOf(user.id) !== -1
      return match
  }
  toBase64=(photo)=>{
    var binstr = Array.prototype.map.call(photo.data.data, function (ch) {
      return String.fromCharCode(ch);
  }).join('');
  const src="data:"+photo.contentType+";base64, "+btoa(binstr); 
  this.imageSrc=src;
  return true
  }
  like=()=>{
      
      const {user,posts,index}=this.props;
      this.props.like(posts[index].like,user.id,user.token,this.props.post._id,index)
     // this.props.getPostData(this.checkLike(post.likes),post.likes.length,post.comments)
  }

  deletePost = () => {
    const {user,posts,index}=this.props;
    this.props.remove(this.props.post._id,user.id,user.token,index)
  }
  render() {
    const {posts,user,index} = this.props;
    const postman=this.props.post;
     if(this.commentList){
  return ( <Card className='card'>
    <CardHeader avatar={
              <Avatar src={profilePic}/>
            }
            action={this.commentList.postedBy._id == user.id &&
              <IconButton onClick={this.deletePost}>
                <DeleteIcon />
              </IconButton>
            }
            title={this.commentList.postedBy.username}
            subheader={(new Date(this.commentList.created)).toDateString()}
            className='cardHeader'/>
        <CardContent className='cardContent'>
          <Typography component="p" className='text'>
            {this.commentList.text}
          </Typography>
          {this.commentList.photo && this.toBase64(this.commentList.photo)&&
            (<div className='photo'>
              <img
                className='media'
                src={this.imageSrc}
                />
            </div>)}
        </CardContent>
        {
          this.props.posts[index]?
        <CardActions>
          
            { this.props.posts[index] && this.props.posts[index].like? <IconButton onClick={this.like} className='button' aria-label="Like" color="secondary">
                <FavoriteIcon />
              </IconButton>
            : <IconButton onClick={this.like} className='button' aria-label="Unlike" color="secondary">
                <FavoriteBorderIcon />
              </IconButton> } <span>{this.props.posts[index].likes}</span>
              <IconButton className='button' aria-label="Comment" color="secondary">
                <CommentIcon/>
              </IconButton> <span>{this.commentList&&this.commentList.comments? this.commentList.comments.length:''}</span>
        </CardActions>:''
        }
        <Divider/>
          {this.commentList?<Comments postId={this.commentList._id} index={this.props.index} comments={this.commentList.comments} updateComments={this.updateComments}/>:''}
          </Card>
    )}
    return(
      <div></div>
    )
  }
}


const mapStateToProps=state=>{
    return(
        {
        user:state.userReducer,
        posts:state.postsReducer
       }
    )
    
}

const mapDispatchToProps=dispatch=>{
    return {
        getPostData: (like,likes,comments,index) => dispatch(actions.getPostData(like,likes,comments,index)),
        like: (like,id,token,postId,index) => dispatch((actions.likePost(like,id,token,postId,index))),
        remove:(postId,id,token,index) => dispatch((actions.deletePost(postId,id,token,index))),
    }
  }

 
export default connect(mapStateToProps,mapDispatchToProps)(Post)
