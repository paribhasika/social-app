export const getCommentsData = (postId,comments)=>{
    return {
        type: 'COMMENT_REQ',
        postId,comments
      }
}

export const deleteComment = (id,token,postId,comment,index)=>{
    return {
        type: 'DELETE_COMMENT',
        id,token,postId,comment,index
      }
}
export const addComment = (id,token,postId,text,index)=>{
    return {
        type: 'ADD_COMMENT',
        id,token,postId,text,index
      }
}

export const handleChange = (text)=>{
    return {
        type: 'HANDLE_CHANGE',
        text
      }
}