import React, {Component} from 'react'
import { CardHeader } from 'material-ui/Card'
import TextField from 'material-ui/TextField'
import Avatar from 'material-ui/Avatar'
import Icon from 'material-ui/Icon'
import PropTypes from 'prop-types'
import {withStyles} from 'material-ui/styles'
import {Link} from 'react-router-dom'
import * as actions from './actions'
import { connect }  from 'react-redux'; 
import profilePic from '../../assets/profile-pic.png'

class Comment extends Component {
   
   componentWillMount(){
    this.array=[];
    this.array.push(this.props.comments);
    this.getComments=this.props.comments,
    this.postId=this.props.postId;
   }
  componentWillReceiveProps(props){
   this.getComments=props.comments,
    this.postId=props.postId
    this.array.push(props.comments);
    
  }
  handleChange = name => event => {
    this.props.handleChange(event.target.value)
  }
  addComment = (event) => {
    const {user} = this.props;
    if(event.keyCode == 13 && event.target.value){
      event.preventDefault()
      this.props.addComment(user.id,user.token,this.postId,this.props.commentsRed.text,this.props.index)
      event.target.value=''
    }
}
deleteComment = comment => event => {
  const {user,index} = this.props;
  this.props.deleteComment(user.id,user.token,this.postId,comment,index)  
 
}
  render() {
    const {user,index}=this.props;
    const commentBody = item => {
      return (
        <p className='commentText'>
          {item.postedBy.username}<br/>
          {item.text}
          <span className='commentDate'>
            {(new Date(item.created)).toDateString()} |
            {user.id == item.postedBy._id &&
              <Icon onClick={this.deleteComment(item)} className='commentDelete'>delete</Icon> }
          </span>
        </p>
      )
    }
   
    return (<div>
        <CardHeader avatar={<Avatar className='smallAvatar' src={profilePic}/>}
              title={ <TextField
                onKeyDown={this.addComment}
                multiline
                onChange={this.handleChange('text')}
                placeholder="Write something ..."
                className='commentField'
                margin="normal"
                />}
              className='cardHeader'/>
        {this.getComments? this.getComments.map((item, i) => {
            return <CardHeader
                      avatar={
                        <Avatar className='smallAvatar' src={profilePic}/>
                      }
                      title={commentBody(item)}
                      className='cardHeader'
                      key={i}/>
              }):''
        }</div>)
  }
}


const mapStateToProps=state=>{
    return(
        {
        user:state.userReducer,
        commentsRed:state.commentsReducer
       }
    )
    
}

const mapDispatchToProps=dispatch=>{
    return {
        getCommentsData:(postId,comments)=> dispatch(actions.getCommentsData(postId,comments)),
        handleChange: (text) => dispatch(actions.handleChange(text)),
        addComment: (like,id,token,postId,index) => dispatch((actions.addComment(like,id,token,postId,index))),
        deleteComment:(id,token,postId,comment,index) => dispatch((actions.deleteComment(id,token,postId,comment,index))),
    }
  }

 
export default connect(mapStateToProps,mapDispatchToProps)(Comment)
