import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import {withStyles} from 'material-ui/styles'
import Paper from 'material-ui/Paper'
import List, {ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText} from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import Button from 'material-ui/Button'
import IconButton from 'material-ui/IconButton'
import Typography from 'material-ui/Typography'
import {Link} from 'react-router-dom'
import Snackbar from 'material-ui/Snackbar'
import ViewIcon from 'material-ui-icons/Visibility'
import {getToFollowList,updateToFollowList,closeFollowMessage} from './actions';
import FollowButton from '../FollowButton';
import {clickFollow} from '../Feed/actions'
import profilePic from '../../assets/profile-pic.png'


class ToFollowList extends Component {
  
  componentDidMount = () => {
    
    this.props.getToFollowList(this.props.user.id,this.props.user.token)
  }
  clickFollowButton = (item, index) => {
        
    const {user,toFollow}=this.props;
    const token=user.token;
    let toFollowUser = toFollow.user
    toFollowUser.splice(index, 1)
    this.props.updateToFollowList(toFollow.user,item.username);
    this.props.clickFollow(
      token,
      user.id,
      item._id,
      item.userFollowing)
  }
  handleRequestClose = (event, reason) => {
    //this.props.toFollow.closeFollowMessage();
  }
  render() {
    return (<div className='to-follow-list'>
      <Paper className='root' elevation={4}>
        <Typography type="title" className='title'>
          Who to follow
        </Typography>
        <List>
          {this.props.toFollow.user?this.props.toFollow.user.map((item, i) => {
              return <span key={i}>
                <ListItem>
                  <ListItemAvatar className='avatar'>
                      <Avatar src={profilePic}/>
                  </ListItemAvatar>
                  <ListItemText primary={item.username}/>
                  <ListItemSecondaryAction className='follow'>
                    <FollowButton onButtonClick={this.clickFollowButton.bind(this, item, i)} />
                    
                  </ListItemSecondaryAction>
                </ListItem>
              </span>
            }):''
          }
        </List>
      </Paper>
       <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          open={this.props.toFollow.open}
          onClose={this.handleRequestClose}
          autoHideDuration={6000}
          message={<span className='snack'>{this.props.toFollow.followMessage}</span>}
      /> 
    </div>)
  }
}

const mapStateToProps=state=>{
    return{
        user:state.userReducer,
        toFollow:state.toFollowReducer
    }
}

export default connect(mapStateToProps,{getToFollowList,clickFollow,updateToFollowList,closeFollowMessage})(ToFollowList)
