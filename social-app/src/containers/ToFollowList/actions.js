export const getToFollowList = (id,token)=>{
    return {
        type: 'TOFOLLOW_LIST_REQ',
        id,
        token
      }
}

export const updateToFollowList = (user,name)=>{
    return {
        type: 'TOFOLLOW_LIST_UPDATE',
        user,
        name
      }
}

export const closeFollowMessage = ()=>{
    return{
        type:'FOLLOW_MESSAGE_CLOSE'
    }
}