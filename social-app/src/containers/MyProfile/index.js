import React, { Component } from 'react';
import MediaCard from '../../component/MediaCard'
import {userProfileReq,clickFollow} from './actions'
import { connect } from 'react-redux';
import Paper from 'material-ui/Paper'
import List, {ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText} from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import IconButton from 'material-ui/IconButton'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import Edit from 'material-ui-icons/Edit'
import Divider from 'material-ui/Divider'
import {Redirect, Link} from 'react-router-dom';
import FollowButton from './../FollowButton'
import MyProfileTabs from './../MyProfileTabs'
import * as actions from './actions'
import profilePic from '../../assets/profile-pic.png'

class MyProfile extends Component {
  
    componentDidMount = () => {
      const jwt = this.props.user.token,
      userId=this.props.user.id;
      this.props.userPostsReq(userId,jwt)
      this.props.userProfileReq(userId,jwt);
      }
   
      removePost = (post) => {
        const updatedPosts = this.props.profile.posts
        const index = updatedPosts.indexOf(post)
        updatedPosts.splice(index, 1)
        //this.setState({posts: updatedPosts})
      }

    render(){
        const {profile,user}=this.props;
        const photoUrl = profilePic
        return(
            <div>
  <Paper className='root' elevation={4}>
    <h3> Profile </h3>
      <List dense>
        <ListItem>
          <ListItemAvatar>
             <Avatar>
             <Avatar src={photoUrl} className='bigAvatar'/>
             </Avatar>
          </ListItemAvatar>
          <ListItemText primary={profile.user.username} secondary={profile.user.email}/>
          {
            this.props.user.id == profile.user._id
             ? (<ListItemSecondaryAction>
                  <Link to={"/user/edit/" + profile.user.id}>
                    <IconButton aria-label="Edit" color="primary">
                      <Edit/>
                    </IconButton>
                  </Link>
                
                </ListItemSecondaryAction>)
            : (<FollowButton following={profile.user.following} onButtonClick={this.clickFollowButton}/>)
            }
        </ListItem>
        <Divider/>
        <ListItem>
          <ListItemText primary={"Joined: " + 
              (new Date(profile.created)).toDateString()}/>
        </ListItem>
      </List>
  </Paper>
  <MyProfileTabs users={this.props.profile.user} posts={this.props.profile.posts} removePostUpdate={this.removePost}/>
  
</div>
        )
    }
 }


 const mapStateToProps = state => ({
    profile: state.myProfileReducer,
    user:state.userReducer,
    follow:state.followReducer        
 })
 const mapDispatchToProps=dispatch=>{
  return {
    userProfileReq: (userId,jwt) => dispatch(actions.userProfileReq(userId,jwt)),
    userPostsReq:(userId,jwt)=>dispatch(actions.userPostsReq(userId,jwt))
 }
}
export default connect(mapStateToProps, mapDispatchToProps)(MyProfile)