export const userProfileReq = (id,jwt)=> {  
  return {
      type: 'USER_PROFILE_REQ',
      id,
      jwt
    }
}
export const userPostsReq =(id,jwt)=>{
  return{
    type: 'USER_POSTS_REQ',
      jwt,
      id
  }
}
export const addProfileComment =(id,jwt)=>{
  return{
    type: 'ADD_COMMENTS_PROFILE',
      jwt,
      id
  }
}
export const deleteProfileComment =(id,jwt)=>{
  return{
    type: 'REMOVE_COMMENTS_PROFILE',
      jwt,
      id
  }
}



