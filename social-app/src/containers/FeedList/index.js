import React, {Component} from 'react'
import Post from '../Posts'
import { connect } from 'react-redux';
class FeedList extends Component  {
  
  componentWillReceiveProps(props){
    this.updatedPosts=props.posts;
  }
  
  render(){
    return (
      <div className='feedsList'>
        {this.updatedPosts&&this.updatedPosts.length>0?this.updatedPosts.map((item, i) => {
            return <Post post={item} key={i} index={i}
                         onRemove={this.props.removeUpdate}/>
          }):''
        }
      </div>
    )
  }
   
    
  }
const mapStateToProps=state=>{
  return{
    feed: state.feedReducer
  }
}
export default connect(mapStateToProps)(FeedList);