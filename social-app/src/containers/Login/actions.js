import React from 'react';

export const loginRequest = ({ email, password })=> {  
    return {
      type: 'REQ_LOGIN',
      email,
      password
    }
}
  
