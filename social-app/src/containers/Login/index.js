import React, { Component } from 'react';
import { reduxForm,Field,handleSubmit } from 'redux-form';  
import { connect } from 'react-redux';
import  {loginRequest}  from './actions';
import {Redirect} from 'react-router-dom';
import CopyContainer from '../../component/CopyContainer'
import {validate} from '../../Helpers/auth'

class Login extends Component {

  
    clickSubmit = (values) => {
      this.props.loginRequest(values)
    }
    
    render () {
      if(this.props.user.isLoggedIn||this.props.user.token){
        const redirectUrl="/profile/"+this.props.user.id
        return (<Redirect to='/feed' />)
      }
      const loginState=this.props.login;
      const { handleSubmit, pristine, submitting } = this.props;
      const {login}=this.props.form
        return (
          <div className="login">
            <form className="login-form" onSubmit={handleSubmit(this.clickSubmit)}>
            <CopyContainer type='heading' content='Sign In'/>
            <div className='field-form'>
              <label htmlFor="email">Email</label>
              <Field name="email" type="text" id="email" className="email" label="Email" component="input" />
              {login.fields&&login.fields.email&&login.syncErrors&&login.syncErrors.email&&<p className='error'>{login.syncErrors.email}</p>}
             </div>
              <div className='field-form'>
              <label htmlFor="password">Password</label>
              <Field name="password" type="password" id="password" className="password" label="Password" component="input"/>
              {login.fields&&login.fields.password&&login.syncErrors&&login.syncErrors.password&&<p className='error'>{login.syncErrors.password}</p>}
             </div>
              <button action="submit" disabled={pristine||submitting}>Login</button>
            </form>
            <div className="auth-messages">
            {!loginState.requesting && !!loginState.errors.length && (
              <div className="error">Failure to login</div>
            )}
            {loginState.requesting && <div>Logging in...</div>}
           </div>
          </div>
        )
      }

}
const mapStateToProps = state => ({
    login: state.loginReducer,
    user:state.userReducer,
    form:state.form
     
 })
     
 
 export default reduxForm({  
     form: 'login',
     validate
   })(connect(mapStateToProps, { loginRequest })(Login))  