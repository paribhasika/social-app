import React from 'react';

export const feedRequest = (id,token)=> {  
    return {
      type: 'REQ_FEED',
      id,
      token
    }
}


export const clickFollow = (token,id,followId,userFollowing)=> {  
  return {
      type: 'CLICK_FOLLOW_REQ',
      token,
      id,
      followId,
      userFollowing
    }
}