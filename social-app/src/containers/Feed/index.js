import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Redirect} from 'react-router'
import {withStyles} from 'material-ui/styles'
import Card from 'material-ui/Card'
import Typography from 'material-ui/Typography'
import Divider from 'material-ui/Divider'
import {feedRequest} from './actions'
import AddPost from '../AddPost'
import { connect } from 'react-redux'; 
import FeedList from '../FeedList'
import ToFollowList from '../ToFollowList'
import * as actions from './actions'

class Newsfeed extends Component {
  
  componentDidMount() {
    const {user}=this.props
    this.props.feedRequest(user.id,user.token)
  }
  componentWillMount() {
    const {user}=this.props
    this.props.feedRequest(user.id,user.token)
  }
  addPost = (post) => {
    const {user}=this.props
  }
  clickFollowButton = () => {
        
    const {user,profile}=this.props;
    const token=user.token
    this.props.clickFollow(
      token,
      user.id,
      profile.user._id,
      profile.user.userFollowing)
  }
  render() {
    if(!this.props.user.isLoggedIn||!this.props.user.token){
      return (<Redirect to='/' />)
      }
    return (
      <div className='feed'>
      <div className='feed-col-one'>
      <Card>
        <h3>Newsfeed </h3>
        <Divider/>
         <AddPost addUpdate={this.addPost}/> 
        <Divider/>
        {this.props.feed?<FeedList removeUpdate={this.removePost} posts={this.props.feed.data}/>:''}
      </Card>
      </div>
      <ToFollowList onButtonClick={this.clickFollowButton}/>
      </div>
    )
  }
}


const mapStateToProps = state => ({
    feed: state.feedReducer,
    user:state.userReducer  
 })
const dispatchStateToProps=dispatch=>{
  return{
    clickFollow:(token,id,profileId,userFollowing)=> dispatch(actions.clickFollow(token,id,profileId,userFollowing)),
    feedRequest:(id,token)=>dispatch(actions.feedRequest(id,token))
  }
}
 
 export default (connect(mapStateToProps, dispatchStateToProps)(Newsfeed))  



 