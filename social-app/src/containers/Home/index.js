import React, { Component } from 'react';
import MediaCard from '../../component/MediaCard'
import { connect } from 'react-redux';

const Home = () =>{
    return(
        <div className='home'>
        <MediaCard title='Home Page' img='./seashell.jpg' desc=' Welcome to the Social Home Page' />
        </div>
    )
 }

  export default Home;
