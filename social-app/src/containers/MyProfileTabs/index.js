import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import Button from 'material-ui/Button'
import {Link} from 'react-router-dom'
import AppBar from 'material-ui/AppBar'
import Typography from 'material-ui/Typography'
import Tabs, { Tab } from 'material-ui/Tabs'
import FollowList from '../../component/FollowList'
import FeedList from './../FeedList'
import * as action from './actions'

class MyProfileTabs extends Component {
  
  handleTabChange = (event, value) => {
    this.props.setTab(value)
  }

  render() {
    return (
    <div>
        <AppBar position="static" color="default">
          <Tabs
            value={this.props.tabProfile.tab}
            onChange={this.handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            fullWidth
          >
            <Tab label="Posts" />
            <Tab label="Following" />
            <Tab label="Followers" />
          </Tabs>
        </AppBar>
       {this.props.tabProfile.tab === 0 && <TabContainer><FeedList removeUpdate={this.props.removePostUpdate} posts={this.props.posts}/></TabContainer>}
       {this.props.tabProfile.tab === 1 && <TabContainer><FollowList people={this.props.users.following}/></TabContainer>}
       {this.props.tabProfile.tab === 2 && <TabContainer><FollowList people={this.props.users.followers}/></TabContainer>}
    </div>)
  }
}

MyProfileTabs.propTypes = {
  users: PropTypes.object.isRequired,
  removePostUpdate: PropTypes.func.isRequired,
  posts: PropTypes.array.isRequired
}

const TabContainer = (props) => {
  return (
    <Typography component="div" style={{ padding: 8 * 2 }}>
      {props.children}
    </Typography>
  )
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
}
const mapStateToProps=state=>({tabProfile:state.profileTabReducer})
const mapDispatchToProps=dispatch=>{
    return {
        setTab:(value)=>dispatch(action.setTab(value))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(MyProfileTabs)
