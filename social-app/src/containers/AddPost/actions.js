import React from 'react';

export const handleChange = (name,value)=> {  
    return {
      type: 'HANDLE_CHANGE',
      name,
      value
    }
}
export const submitPost = (id,token,postData)=> {  
    return {
      type: 'POST_SUBMIT',
      id,
      token,
      postData
    }
}
export const loadUser = (user)=> {  
    return {
      type: 'REQ_USER',
      user
    }
}
  
