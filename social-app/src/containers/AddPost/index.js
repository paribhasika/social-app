import React, {Component} from 'react'
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import Typography from 'material-ui/Typography'
import Avatar from 'material-ui/Avatar'
import Icon from 'material-ui/Icon'
import PropTypes from 'prop-types'
import {withStyles} from 'material-ui/styles'
import { connect } from 'react-redux'; 
import IconButton from 'material-ui/IconButton'
import PhotoCamera from 'material-ui-icons/PhotoCamera'
import * as actions from './actions';
import profilePic from '../../assets/profile-pic.png';


class AddPost extends Component {
  

  componentDidMount ()  {
    this.postData = new FormData()
    const {user}=this.props;
    this.props.loadUser(user)
  }
  clickPost = () => {
    const {user}=this.props;
    //const jwt = auth.isAuthenticated()
    this.props.submitPost(user.id,user.token,this.postData)
    this.props.addUpdate(this.postData)
  }
  handleChange = name => event => {
    const value = name === 'photo'
      ? event.target.files[0]
      : event.target.value
    this.postData.set(name, value);

    this.props.handleChange(name, value);
  }
  
  render() {
    const {addPost}=this.props;
   return (<div className='root'>
      <Card className='card'>
      <CardHeader
            avatar={
              <Avatar src={profilePic}/>
            }
            title={addPost.user.username}
            className='cardHeader'
          />
      <CardContent className='cardContent'>
        <TextField
            placeholder="Share your thoughts ..."
            multiline
            rows="3"
            value={addPost.text}
            onChange={this.handleChange('text')}
            className='textField'
            margin="normal"
        />
        <input accept="image/*" onChange={this.handleChange('photo')} className='input' id="icon-button-file" type="file" />
        <label htmlFor="icon-button-file">
          <IconButton color="secondary" className='photoButton' component="span">
            <PhotoCamera />
          </IconButton>
        </label> <span className='filename'>{addPost.photo ? addPost.photo.name : ''}</span>
        { addPost.error && (<Typography component="p" color="error">
            <Icon color="error" className='error'>error</Icon>
              {addPost.error}
            </Typography>)
        }
      </CardContent>
      <CardActions>
        <Button color="primary" variant="raised" disabled={addPost.text === ''} onClick={this.clickPost} className='submit'>POST</Button>
      </CardActions>
    </Card>
  </div>)
  }
}

const mapStateToProps=state=>{
return({
  addPost:state.addPostReducer,
  user:state.userReducer
})
}
const mapDispatchToProps=dispatch=>{
    return {
        handleChange: (name,value) => dispatch(actions.handleChange(name,value)),
        submitPost: (id,token,postData) => dispatch(actions.submitPost(id,token,postData)),
        loadUser:(user)=>dispatch((actions.loadUser(user)))
    }
  }


export default connect(mapStateToProps,mapDispatchToProps) (AddPost)
