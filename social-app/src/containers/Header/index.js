import React, { Component } from 'react';
import { connect } from 'react-redux';
import {signOut} from '../../Helpers/auth'
import {Link, withRouter,Redirect} from 'react-router-dom';
import CopyContainer from '../../component/CopyContainer'
import IconButton from 'material-ui/IconButton'
import HomeIcon from 'material-ui-icons/Home'
import Button from 'material-ui/Button';

const isActive = (history, path) => {
  if (history.location.pathname == path)
    return {color: '#ff4081'}
  else
    return {color: '#ffffff'}
}
class Header extends Component    {
  
  render () {
    const {user} = this.props;
    return(
    <div className='header'>
      <CopyContainer type="heading" content="Social App" />
      <Link to="/">
        <IconButton aria-label="Home" style={isActive(this.props.history, "/")}>
          <HomeIcon/>
        </IconButton>
      </Link>
     
      {
        !user.token && (<span>
          <Link to="/signup">
            <Button style={isActive(this.props.history, "/signup")}>Sign up
            </Button>
          </Link>
          <Link to="/login">
            <Button style={isActive(this.props.history, "/login")}>Sign In
            </Button>
          </Link>
        </span>)
      }
      {
        user.token && (<span>
           <Link to="/feed">
            <Button style={isActive(this.props.history, "/feed")}>Feed</Button>
        </Link>
          <Link to={"/profile/" + user.id}>
            <Button style={isActive(this.props.history, "/profile/" + user.id)}>My Profile</Button>
          </Link>
          <Button color="inherit" onClick={() => {signOut(this.props.dispatch) 
            }}>Sign out</Button>
        </span>)
      }
    </div>
    )

  }
}

const mapStateToProps=state=>{
  return{
    user:state.userReducer
  }
}
export default withRouter(connect(mapStateToProps)(Header));
