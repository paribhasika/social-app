export function setUser (token,id) {  
    return {
      type: 'USER_SET',
      token,
      id
    }
  }
  
  export function unsetUser () { 
    return {
      type: 'USER_UNSET',
    }
  }