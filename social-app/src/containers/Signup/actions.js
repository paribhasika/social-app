import React from 'react';

export const signupRequest = ({ email, password , username })=> {  
    return {
      type: 'REQ_SIGNUP',
      email,
      password,
      username
    }
}
  
