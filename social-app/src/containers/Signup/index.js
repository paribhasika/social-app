import React, { Component } from 'react';
import { reduxForm,Field,handleSubmit } from 'redux-form';  
import { connect } from 'react-redux';
import  {signupRequest}  from './actions';
import {Redirect} from 'react-router-dom';
import MyProfile from '../MyProfile';
import CopyContainer from '../../component/CopyContainer'
import {validate} from '../../Helpers/auth'

class Signup extends Component {
    clickSubmit = (values) => {
        this.props.signupRequest(values)
      }
    
      render () {
        if(this.props.user.isLoggedIn||this.props.user.token){
          const redirectUrl="/profile/"+this.props.user.id
            return (<Redirect to='/feed' />)
          }
          const signupState=this.props.signup;
          const { handleSubmit, pristine, submitting } = this.props;
          const {signup}=this.props.form
        return (
          <div className="signup">
            <form className="signup-form" onSubmit={handleSubmit(this.clickSubmit)}>
            <CopyContainer type='heading' content='Sign Up'/>
            <div className='field-form'>
              <label htmlFor="email">Email</label>
              <Field name="email" type="text" id="email" className="email" label="Email" component="input" />
              {signup.fields&&signup.fields.email&&signup.syncErrors&&signup.syncErrors.email&&<p className='error'>{signup.syncErrors.email}</p>}
              </div>
              <div className='field-form'>
              <label htmlFor="name">Name</label>
              <Field name="username" type="text" id="username" className="username" label="UserName" component="input" />
              {signup.fields&&signup.fields.username&&signup.syncErrors&&signup.syncErrors.username&&<p className='error'>{signup.syncErrors.username}</p>}
              </div>
              <div className='field-form'>
              <label htmlFor="password">Password</label>
              <Field name="password" type="password" id="password" className="password" label="Password" component="input"/>
              {signup.fields&&signup.fields.password&&signup.syncErrors&&signup.syncErrors.password&&<p className='error'>{signup.syncErrors.password}</p>}
              </div>
              <button action="submit" disabled={pristine||submitting}>SIGNUP</button>
            </form>
            <div className="auth-messages">
              {!signupState.requesting && !!signupState.errors.length && (
                <div className="error">Failure to signup</div>)}
          
          {signupState.requesting && <div>Signing in...</div>}
          
        </div>
            
          </div>
         

        )
      }

}
const mapStateToProps = state => ({
   signup: state.signupReducer,
   user:state.userReducer,
   form:state.form
    
})
    

export default reduxForm({  
    form: 'signup',
    validate
  })(connect(mapStateToProps, { signupRequest })(Signup))  