import Button from 'material-ui/Button';
import React, { Component } from 'react';

class FollowButton extends Component {
    followClick = () => {
     this.props.onButtonClick('follow')
    }
    unfollowClick = () => {
      this.props.onButtonClick('unFollow')
    }
    
    render() {
      return (<div>
        { this.props.following
          ? (<Button variant="raised" color="secondary" onClick=
         {this.unfollowClick}>Unfollow</Button>)
          : (<Button variant="raised" color="primary" onClick=
         {this.followClick}>Follow</Button>)
        }
      </div>)
    }
  }

  const mapStateToProps = state=>{
    return {follow:state.followReducer}
  }
  export default FollowButton;