export const clickFollow = (id)=> {  
    return {
        type: 'CLICK_FOLLOW',
        id
      }
  }

  export const clickUnFollow = (id)=> {  
    return {
        type: 'CLICK_UNFOLLOW',
        id
      }
  }