import React, {Component} from 'react';
import {Route, Redirect} from 'react-router-dom';
import {checkAuthorization} from '../Helpers/auth';
import MyProfile from '../containers/MyProfile';
const PrivateRoute = ({component: Component,authed,dispatch,redirectPath,...rest}) =>{
    
        if(checkAuthorization(authed,dispatch)){
            return (
                <Route {...rest} component={MyProfile} /> 
            )
           }
           else{
            return (
                 <Redirect to="/"/>
            )
           }
    
   
    
} 
export default PrivateRoute;