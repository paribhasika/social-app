import React, { Component } from 'react';
import {Route,Switch,Redirect,withRouter} from 'react-router-dom';
import {checkAuthorization} from '../Helpers/auth'
import { connect } from 'react-redux';
// Import all of our components
import Login from '../containers/Login';  
import Signup from '../containers/Signup';
import Home from '../containers/Home';
import MyProfile from '../containers/MyProfile';
import Feed from '../containers/Feed';
import PrivateRoute from '../PrivateRoute';

class Main extends Component {

  render() {
   checkAuthorization(this.props.user,this.props.dispatch)
   return (
      <div className='main-container'>
      <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/login"  component={Login} />
            <Route path="/signup" component={Signup} />
            <Route path="/feed" component={Feed} />
            <PrivateRoute authed={this.props.user} dispatch={this.props.dispatch} path="/profile/:userId" redirectPath='/login' component={MyProfile} />
            <Redirect to="/" />
     </Switch>
      </div>  
    );
  }
}

const mapStateToProps=state=> {
  return { user: state.userReducer };
}

export default withRouter(connect(mapStateToProps)(Main))
