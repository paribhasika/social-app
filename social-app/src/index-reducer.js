import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import userReducer from './reducers/userReducer'  
import signupReducer from './reducers/signupReducer'
import loginReducer from './reducers/loginReducer'
import myProfileReducer from './reducers/myProfileReducer'
import toFollowReducer from './reducers/toFollowReducer'
import postsReducer from './reducers/postsReducer'
import feedReducer from './reducers/feedReducer'
import addPostReducer from './reducers/addPostReducer'
import commentsReducer from './reducers/commentsReducer'
import profileTabReducer  from './reducers/myProfileTabsReducers'
const IndexReducer = combineReducers({
  userReducer, 
  signupReducer, 
  loginReducer,
  myProfileReducer,
  toFollowReducer,
  postsReducer,
  feedReducer,
  addPostReducer,
  commentsReducer,
  profileTabReducer,
  form,
})

export default IndexReducer