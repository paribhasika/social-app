import React, { Component } from 'react';
import {setUser,unsetUser} from '../containers/User/actions'
import {Redirect} from 'react-router-dom'


  
const checkTokenAuthorization = (dispatch)=> {  
    const storedToken = localStorage.getItem('token')
  
    if (storedToken) {
      const token = storedToken;
  
      const createdDate = new Date(token.created)
      const created = Math.round(createdDate.getTime() / 1000)
      const ttl = 1209600
      const expiry = created + ttl
  
      if (created > expiry) return false
      if(localStorage.getItem('id')){
        const id=localStorage.getItem('id');
        dispatch(setUser(token,id))
      }
      
      
      return true
    }
  
    return false
  }

  export function checkAuthorization (user,dispatch) { 
    
     if (user && user.token) {
        return true;
      }
      if (checkTokenAuthorization(dispatch)){
        return true;
      }
  return false
  
      
}
export function validate (values) {
  const errors = {}
  if (!values.username) {
    errors.username = 'Required'
  } 
  if (!values.email) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }
  if (!values.password) {
    errors.password = 'Required'
  } 
  return errors
} 
export function signOut(dispatch){
  dispatch(unsetUser())
  localStorage.removeItem('token')
  localStorage.removeItem('id');
  <Redirect to='/' />
  
}