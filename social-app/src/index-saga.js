import SignupSaga from './sagas/signupSagas';
import LoginSaga from './sagas/loginSagas';
import MyProfileSaga from './sagas/myProfileSagas';
import FollowSaga from './sagas/followSagas'
import ToFollowSaga from './sagas/toFollowSagas'
import FeedSaga from './sagas/feedSagas'
import PostsSaga from './sagas/postsSagas'
import AddPostSaga from './sagas/addPostSagas'
import CommentSaga from './sagas/commentsSagas'
export default function* IndexSaga () {  
    yield [SignupSaga(),LoginSaga(),MyProfileSaga(),FollowSaga(),ToFollowSaga(),FeedSaga(),PostsSaga(),AddPostSaga(),CommentSaga()]
  }