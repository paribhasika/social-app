import React from 'react';
 
export  const config ={
    signupUrl:'http://localhost:8081/auth/signup' ,
    loginUrl:'http://localhost:8081/auth/signin',
    myProfileUrl:'http://localhost:8081/users' ,
    followUserUrl:'http://localhost:8081/users/follow',
    unFollowUserUrl:'http://localhost:8081/users/unfollow',
    toFollowList:'http://localhost:8081/users/findpeople',
    fetchFeedUrl:'http://localhost:8081/posts/feed',
    fetchFeedUserUrl:'http://localhost:8081/posts/feed/by',
    addNewFeedUrl:'http://localhost:8081/posts/feed/new',
    deletePostUrl:'http://localhost:8081/posts',
    likeUrl:'http://localhost:8081/posts/like',
    unlikeUrl:'http://localhost:8081/posts/unlike',
    addCommentsUrl:'http://localhost:8081/posts/comment',
    removeCommentsUrl:'http://localhost:8081/posts/removecomment'
}