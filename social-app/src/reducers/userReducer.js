const initialState = {  
    id: null,
    token: null,
    user:null,
    isLoggedIn:false
  }

  const reducer = (state = initialState, action)=> {  
    switch (action.type) {
      case 'USER_SET':
        return {
          id: action.id,
          token: action.token,
          isLoggedIn:true,
          user:action.user
        }
  
      case 'USER_UNSET':
        return {
          id: null,
          token: null,
          isLoggedIn:false,
          user:null
        }
  
      default:
        return state
    }
  }
  
  export default reducer  