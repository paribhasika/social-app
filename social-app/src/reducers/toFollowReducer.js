const initialState = {  
    user:'',
    open:false,
    followMessage:''
  }
  const toFollowReducer = (state = initialState, action)=> {  
    switch (action.type) {
      case 'TOFOLLOW_LIST_REQ':
        return {
          ...state,
          requesting: true,
          successful: false,
          messages: [{ body: 'Getting Profile', time: new Date() }],
          errors: [],
        }
    case 'TOFOLLOW_LIST_SUCCESS':
      return (
        {...state,
        user:action.response}
      );
    case 'TOFOLLOW_LIST_ERROR':
      return {
        ...state,
        errors: state.errors.concat([{
          body: action.error.toString(),
          time: new Date(),
        }]),
        messages: [],
        requesting: false,
        successful: false,
      }
      case 'TOFOLLOW_LIST_UPDATE':
       return {
          ...state,
          user:action.user,
          open:true,
          followMessage:'Following' +action.name+'!'
        }
        case 'FOLLOW_MESSAGE_CLOSE':
        return{
          ...state,
          open:false
        }
      default:
        return state
    }
}
  
export default toFollowReducer  