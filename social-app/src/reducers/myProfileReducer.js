const initialState = {  
    user:{
      email:'',
      followers:[''],
      following:[''],
      username:''
    },
    userFollowing:'',
    posts:[]
  }
  const myProfileReducer = (state = initialState, action)=> {  
    switch (action.type) {
      case 'USER_PROFILE_REQ':
        return {
          ...state,
          requesting: true,
          successful: false,
          messages: [{ body: 'Getting Profile', time: new Date() }],
          errors: [],
        }
    case 'USER_PROFILE_SUCCESS':
      return (
        {...state,
        user:action.user}
      );
    case 'USER_PROFILE_ERROR':
      return {
        ...state,
        errors: state.errors.concat([{
          body: action.error.toString(),
          time: new Date(),
        }]),
        messages: [],
        requesting: false,
        successful: false,
      }
      case 'CLICK_FOLLOW_REQ':
        return {
          ...state,
          requesting: true,
          successful: false,
          messages: [{ body: 'Setting Follow', time: new Date() }],
          errors: [],
        }
    case 'CLICK_FOLLOW_SUCCESS':
      return (
        {...state,
        userFollowing:!state.userFollowing
        }
      );
    case 'CLICK_FOLLOW_ERROR':
      return {
        ...state,
        errors: state.errors.concat([{
          body: action.error.toString(),
          time: new Date(),
        }]),
        messages: [],
        requesting: false,
        successful: false,
      }
      case 'CHECK_FOLLOW':
      return {
        ...state,
        userFollowing:action.userFollowing
      }
      case 'USER_POSTS_REQ':return {
        ...state
      }
      case 'USER_POSTS_SUCCESS':
      return {
        ...state,
        posts:action.response
      }
      case 'REMOVE_POST_PROFILE':
      const updatedData=[...state.posts];
      state.posts?updatedData.splice(action.index,1):'';
      return {
        ...state,
        posts:updatedData
    }
      case 'UPDATE_COMMENTS_PROFILE':
      const tempData=[...state.posts];
      const updated=tempData.map(list=>list._id==action.response._id?action.response:list)
      return {
          ...state,
           posts: updated
      }
    
    
      
      default:
        return state
    }
}
  
export default myProfileReducer  