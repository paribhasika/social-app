const initialState = {
    text: '',
    photo: '',
    error: '',
    user: {}
  }


const addPostReducer = (state = initialState, action)=> {  
    switch (action.type) {
      case 'POST_SUBMIT':
      return{
        ...state,
        loading:true
      }
      case 'POST_SUBMIT_FAILED':
      return {
        ...state,
        loading: false,
        data: [],
        error:action.error
    };
    case 'POST_SUBMIT_SUCCESS':
    return {
        ...state,
        text:'',
        photo:'',
        action:''
    }
    case 'REQ_USER':
    return {
        ...state,
        user:action.user
    }
    case 'HANDLE_CHANGE':
    if(action.name=='text'){
      return {
        ...state,
        text:action.value
    }
    }
    else if(action.name=='photo')
    return {
      ...state,
      photo:action.value
  }
    default:
        return state
    }
}
  
export default addPostReducer  