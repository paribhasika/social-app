const initialState = {  
    data:'',
    loading: false
  }

const feedReducer = (state = initialState, action)=> {  
    switch (action.type) {
      case 'REQ_FEED':
      case 'REQ_FEED_FAILED':
      return {
        ...state,
        loading: true,
        data: []
    };
    case 'REQ_FEED_SUCCESS':
    return {
        ...state,
        loading: true,
        data: action.response
    }
    case 'ADD_POST':
    return {
        ...state,
         data: [action.response,...state.data]
    }
    case 'ADD_COMMENT_FEED':
    const tempData=[...state.data]
    const updated=tempData.map(list=>list._id==action.response._id?action.response:list)
    return {
        ...state,
         data: updated
    }
    case 'REMOVE_POST':
    const updatedData=state.data;
     state.data?updatedData.splice(action.index,1):'';
    return {
        ...state,
        updatedData
    }
    default:
        return state
    }
}
  
export default feedReducer  