const initialState = {  
    requesting: false,
    successful: false,
    messages: [],
    errors: []
  }

const loginReducer = (state = initialState, action)=> {  
    switch (action.type) {
      case 'REQ_LOGIN':
        return {
          requesting: true,
          successful: false,
          messages: [{ body: 'Signing up...', time: new Date() }],
          errors: [],
        }
    case 'LOGIN_SUCCESS':
      return {
        errors: [],
        messages: [{
          body: `Successfully logged in`,
          time: new Date(),
        }],
        requesting: false,
        successful: true,
      }

    case 'LOGIN_ERROR':
      return {
        errors: state.errors.concat([{
          body: action.error.toString(),
          time: new Date(),
        }]),
        messages: [],
        requesting: false,
        successful: false,
      }

  
      default:
        return state
    }
}
  
export default loginReducer  