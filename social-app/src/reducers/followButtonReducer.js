const initialState = {  
    user:{
      email:'',
      followers:[],
      following:[],
      username:''
    }
  }
  const followReducer = (state = initialState, action)=> {  
    switch (action.type) {
      case 'CLICK_FOLLOW_REQ':
        return {
          ...state,
          requesting: true,
          successful: false,
          messages: [{ body: 'Setting Follow', time: new Date() }],
          errors: [],
        }
     case 'CLICK_FOLLOW_SUCCESS':
      return (
        {...state,
        user:{
            ...state.user,
            following:action.following
        }}
      );
     case 'CLICK_FOLLOW_ERROR':
      return {
        ...state,
        errors: state.errors.concat([{
          body: action.error.toString(),
          time: new Date(),
        }]),
        messages: [],
        requesting: false,
        successful: false,
      }

      
      default:
        return state
    }
}
  
export default followReducer  