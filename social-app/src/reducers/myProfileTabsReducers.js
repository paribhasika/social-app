const initialState = {  
    tab:0
  }

const profileTabReducer = (state = initialState, action)=> {  
    switch (action.type) {
      case 'SET_TAB':
        return {
         tab:action.value
        }
    
      default:
        return state
    }
}
  
export default profileTabReducer  