const initialState = {
  text:'',
  commentList:''
}
 
  const commentsReducer = (state = initialState, action)=> {  
    switch (action.type) {
      case 'COMMENT_REQ':
      return {...state,
           commentList:[...state.commentList,{postId:action.postId,comments:action.comments}]} 
        
    case 'ADD_COMMENT':
      return {...state}
      
    case 'ADD_COMMENT_SUCCESS':
      return{...state,
        text:''} 
      case 'ADD_COMMENT_ERROR':
      return([
          ...state]
       )
      case 'DELETE_COMMENT':
      return{...state}
          
       case 'DELETE_COMMENT_SUCCESS':
      
       return{...state,
        text:''} 
        case 'DELETE_COMMENT_ERROR':
       return[ 
          ...state]
        case 'HANDLE_CHANGE':
        return {...state,text:action.text}
      default:
        return state
    }
}
  
export default commentsReducer