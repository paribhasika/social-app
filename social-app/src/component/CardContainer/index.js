import React, { Component } from 'react';
const cardContainer = props =>{
    return(
        <div className={props.className}>
            {props.children}
        </div>
    )
}

export default mediaCard