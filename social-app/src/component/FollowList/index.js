import React, { Component } from 'react';
import PropTypes from 'prop-types';
import List, {ListItem, ListItemAvatar, ListItemIcon, ListItemSecondaryAction, ListItemText} from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import {Link} from 'react-router-dom'
import GridList, { GridListTile } from 'material-ui/GridList';
import profilePic from '../../assets/profile-pic.png';

const followList=props=> {
    
     return (<div className='root'>
        <GridList cellHeight={160} className='gridList' cols={4}>
          {props.people?props.people.map((person, i) => {
             return <GridListTile style={{'height':120}} key={i}>
                <Link to={"/user/" + person._id}>
                  <Avatar src={profilePic} className=
                 'bigAvatar'/>
                  <Typography className='tileText'>{person.username}
                 </Typography>
                </Link>
              </GridListTile>
          }):''}
        </GridList>
      </div>)
    }
  
  
  export default followList;