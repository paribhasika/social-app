import React, { Component } from 'react';
import CopyContainer from '../CopyContainer'
const mediaCard = props =>{
    return(
        <div>
            <CopyContainer type='heading' content={props.title}/>
            <img src={props.img} />
            <CopyContainer type='desc' content={props.desc}/>
        </div>
    )
}

export default mediaCard