import React, { Component } from 'react';
const copyContainer = props =>{
    const Copy = props.type=='heading'?'h2':'p';
    
    return(
            <Copy>
             {props.content}
            </Copy>
       
    )
}

export default copyContainer;