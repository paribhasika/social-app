import { take, fork, cancel, call, put, cancelled,takeLatest } from 'redux-saga/effects'
import { browserHistory } from 'react-router'    
import {config} from '../config/keys'
import {setUser,unsetUser} from '../containers/User/actions'

function* loginFlow (action) {
    try {

        const { email, password } = action
    
        const response = yield call(loginApi, email, password);
        const token=response.token;
        const user= response.user;
         yield put({ type: 'LOGIN_SUCCESS', response })
         yield put(setUser(token,user.userId,user))
         localStorage.setItem('token', token);
         localStorage.setItem('id', user.userId);
         localStorage.setItem('user', user);
        
      } catch (error) {
        yield put({ type: 'LOGIN_ERROR', error })
      }
}


function* loginWatcher () {  
    yield takeLatest('REQ_LOGIN', loginFlow)

    
}

function loginApi (email, password) {
    return fetch(config.loginUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email, password }),
      })
        .then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}
function* logout () {  
    yield put(unsetUser())
  
    // remove our token
    localStorage.removeItem('token')
  
    // redirect to the /login screen
  }
export default loginWatcher  