import { call, put, takeLatest } from 'redux-saga/effects';
import { browserHistory } from 'react-router'    
import {config} from '../config/keys'

function* postsFlow (action) {
    try {

        const { like,id,token,postId,index} = action;
       
        const callApi=(like)?config.unlikeUrl:config.likeUrl;
       const response = yield call(postApi,callApi,id,postId,token);
        const likes=response.likes.length;
        yield put({ type: 'LIKE_POST_SUCCESS',likes,index })
        // yield put({ type: 'POST_REQ',responseLike,likesLength,comments })
      } catch (error) {
        yield put({ type: 'LIKE_POST_ERROR', error })
      }
}

function* deletePostsFlow (action) {
  try {

      const { postId,id,token,index} = action;
     
      const response = yield call(deletePostApi,postId,token);
      const likes=response.likes.length;
      yield put({ type: 'DELETE_POST_SUCCESS',index })
      yield put ({type:'REMOVE_POST',index})
      yield put ({type:'REMOVE_POST_PROFILE',index})
    } catch (error) {
      yield put({ type: 'DELETE_POST_ERROR', error })
    }
}
function* postWatcher () {  
  yield takeLatest('LIKE_POST', postsFlow)
  yield takeLatest('DELETE_POST', deletePostsFlow)
}

function postApi (apiUrl,userId,postId,token) {  
    return fetch(apiUrl, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({userId, postId})
      }).then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}
function deletePostApi (postId,token) {  
  return fetch(config.deletePostUrl+'/'+postId, {
      method: 'DELETE',
      headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + token
      }}).then(response => response.json())
      .then(json => json)
      .catch((error) => { throw error })
}


export default postWatcher  