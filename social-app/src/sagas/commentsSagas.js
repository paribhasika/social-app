import { call, put, takeLatest } from 'redux-saga/effects';
import { browserHistory } from 'react-router'    
import {config} from '../config/keys'

function* addCommentFlow (action) {
    try {

        const { id,token,postId,text,index} = action;
        const response = yield call(addCommentApi,id,postId,{'text': action.text},token);
       const comments=response.comments;
        yield put({ type: 'ADD_COMMENT_SUCCESS',comments ,index})
        yield put({type:'ADD_COMMENT_FEED',response})
        yield put({type:'UPDATE_COMMENTS_PROFILE',response})
        yield put ({type:'UPDATE_COMMENTS',comments,index,response})
        
        
      } catch (error) {
        yield put({ type: 'ADD_COMMENT_ERROR', error })
      }
}

function* deleteCommentFlow (action) {
  try {

      const { postId,id,comment,token,index} = action;
     
      const response = yield call(deleteCommentApi,id,postId,comment,token);
      const comments=response.comments;
      yield put({ type: 'DELETE_COMMENT_SUCCESS',index,comments })
      yield put({type:'ADD_COMMENT_FEED',response})
      yield put({type:'UPDATE_COMMENTS_PROFILE',response})
      yield put ({type:'UPDATE_COMMENTS',comments,index,response})
    } catch (error) {
      yield put({ type: 'DELETE_COMMENT_ERROR', error })
    }
}
function* postWatcher () {  
  yield takeLatest('ADD_COMMENT', addCommentFlow)
  yield takeLatest('DELETE_COMMENT', deleteCommentFlow)
}

function addCommentApi (userId, postId,comment,token) {  
  return fetch(config.addCommentsUrl, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' +token
    },
    body: JSON.stringify({userId, postId,comment})
  }).then((response) => {
    return response.json()
  }).catch((err) => {
    console.log(err)
  })
}
function deleteCommentApi (userId, postId,comment,token) {  
  return fetch(config.removeCommentsUrl, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' +token
    },
    body: JSON.stringify({userId, postId,comment})
  }).then((response) => {
    return response.json()
  }).catch((err) => {
    console.log(err)
  })
}


export default postWatcher  