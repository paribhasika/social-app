import { call, put, takeLatest } from 'redux-saga/effects';
import { browserHistory } from 'react-router'    
import {config} from '../config/keys'

function* toFollowFlow (action) {
    try {

        const { token,id} = action;
        const response = yield call(toFollowApi,id,token);
         yield put({ type: 'TOFOLLOW_LIST_SUCCESS', response })
      } catch (error) {
        yield put({ type: 'TOFOLLOW_LIST_ERROR', error })
      }
}


function* toFollowWatcher () {  
  yield takeLatest('TOFOLLOW_LIST_REQ', toFollowFlow)
}

function toFollowApi (userId,token) {  
    return fetch(config.toFollowList+'/'+userId, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token
        }
      }).then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}


export default toFollowWatcher  