import { call, put, takeLatest } from 'redux-saga/effects'    
import {config} from '../config/keys'
import {setUser} from '../containers/User/actions'

function* signupFlow (action) {
    try {
        const { email, password, username } = action
        const response = yield call(signupApi, email, password, username)
        const token=response.token;
        const user= response.user;
         yield put({ type: 'SIGNUP_SUCCESS', response })
         yield put(setUser(token,user.userId))
         localStorage.setItem('token', token);
         localStorage.setItem('id', user.userId);
      } catch (error) {
        yield put({ type: 'SIGNUP_ERROR', error })
      }
}


function* signupWatcher () {  
  
  yield takeLatest('REQ_SIGNUP', signupFlow)
}

function signupApi (email, password, username) {
   return fetch(config.signupUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password, username }),
      })
        .then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}

export default signupWatcher  