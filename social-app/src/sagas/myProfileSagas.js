import { call, put, takeLatest } from 'redux-saga/effects';
import { browserHistory } from 'react-router'    
import {config} from '../config/keys'
import {checkFollow} from '../Helpers/follower'

function* profileFlow (action) {
    try {

        const { id,jwt } = action;
        const response = yield call(profileApi, id,jwt);
        const user=response;
         yield put({ type: 'USER_PROFILE_SUCCESS', user })
         const userFollowing = checkFollow(user)
         yield put({ type: 'CHECK_FOLLOW', userFollowing })

      } catch (error) {
        yield put({ type: 'USER_PROFILE_ERROR', error })
      }
}

function* userPostsFlow (action) {
  try {

      const { id,jwt } = action;
      yield put({ type: 'RESET_POST' })
    
      const response = yield call(userPostApi, id,jwt);
      const user=response;
      yield put({ type: 'USER_POSTS_SUCCESS', response })
     } catch (error) {
      yield put({ type: 'USER_POSTS_ERROR', error })
    }
}


function* profileWatcher () {  
  yield takeLatest('USER_PROFILE_REQ', profileFlow)
  yield takeLatest('USER_POSTS_REQ', userPostsFlow)
}

function profileApi (id,jwt) {
  
    return fetch(config.myProfileUrl+'/'+id, {
        method: 'GET',
        headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + jwt
        }
      })
        .then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}
function userPostApi (id,jwt) {
  
  return fetch(config.fetchFeedUserUrl+'/'+id, {
      method: 'GET',
      headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + jwt
      }
    })
      .then(response => response.json())
      .then(json => json)
      .catch((error) => { throw error })
}

export default profileWatcher  