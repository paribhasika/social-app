import { call, put, takeLatest } from 'redux-saga/effects'    
import {config} from '../config/keys'

function* addPostFlow (action) {
    try {
        const {id,token,postData}=action
        const response = yield call(addPostApi,id,token,postData);
        yield put({ type: 'POST_SUBMIT_SUCCESS', response });
        yield put({type:'ADD_POST',response});
      } catch (error) {
        yield put({ type: 'POST_SUBMIT_FAILED', error })
      }
}


function* addPostWatcher () {  
  
  yield takeLatest('POST_SUBMIT', addPostFlow)
}

function addPostApi (id,token,postData) {
   return fetch(config.addNewFeedUrl+'/'+id, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token
         },
        body: postData
       })
        .then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}

export default addPostWatcher  