import { call, put, takeLatest } from 'redux-saga/effects';
import { browserHistory } from 'react-router'    
import {config} from '../config/keys'
import {setUser,unsetUser} from '../containers/User/actions'

function* profileFlow (action) {
    try {

        const { id } = action
        const response = yield call(profileApi, id);
        const user=response;
         yield put({ type: 'USER_PROFILE_SUCCESS', user })
      } catch (error) {
        yield put({ type: 'USER_PROFILE_ERROR', error })
      }
}


function* profileWatcher () {  
  yield takeLatest('USER_PROFILE_REQ', profileFlow)
}

function profileApi (token) {
  
    return fetch(config.myProfileUrl+'/'+token, {
        method: 'GET'
      })
        .then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}

export default profileWatcher  