import { call, put, takeLatest } from 'redux-saga/effects'    
import {config} from '../config/keys'

function* getFeedsFlow (action) {
    try {
        const {id,token}=action
        const response = yield call(feedApi,id,token);
        yield put({type:'RESET_POST'})
        yield put({ type: 'REQ_FEED_SUCCESS', response })

      } catch (error) {
        yield put({ type: 'REQ_FEED_ERROR', error })
      }
}


function* feedWatcher () {  
  
  yield takeLatest('REQ_FEED', getFeedsFlow)
}

function feedApi (id,token) {
   return fetch(config.fetchFeedUrl+'/'+id, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + token
          }
       })
        .then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}

export default feedWatcher  