import { call, put, takeLatest } from 'redux-saga/effects';
import { browserHistory } from 'react-router'    
import {config} from '../config/keys'

function* followUserFlow (action) {
    try {

        const {  token,id,followId,userFollowing} = action;
        const response='';
        if(userFollowing){
            const response = yield call(unFollowUserApi, id,followId,token);
        }
        else{
            const response = yield call(followUserApi, id,followId,token);
            yield put({type:'TOFOLLOW_LIST_REQ',id,token})
        }
         
        const user=response;
         yield put({ type: 'CLICK_FOLLOW_SUCCESS', user })
      } catch (error) {
        yield put({ type: 'CLICK_FOLLOW_ERROR', error })
      }
}


function* followWatcher () {  
  yield takeLatest('CLICK_FOLLOW_REQ', followUserFlow)
}

function followUserApi (userId,followId,token) {  
    return fetch(config.followUserUrl, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({ userId,followId })
      }).then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}
function unFollowUserApi (userId,unFollowId,token) {
  
    return fetch(config.unFollowUserUrl, {
        method: 'PUT',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({ userId,unFollowId })
      })
        .then(response => response.json())
        .then(json => json)
        .catch((error) => { throw error })
}

export default followWatcher  