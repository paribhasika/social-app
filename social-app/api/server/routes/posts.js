const express = require('express');
const router = express.Router();
const formidable = require('formidable')
const config = require('../../database/db');
const Post = require('../../models/post-model');
const User = require('../../models/user');
const jwt = require('jsonwebtoken');
const authControl=require('../../controllers/auth');
const postControl=require('../../controllers/posts');
const fs=require('fs');

router.get('/posts/feed/:userId',authControl.confirmSignIn,function(req,res,next){
  let following = req.profile.following;
  following.push(req.profile._id)
  console.log('following:       ',following,req.profile.following);
  
  Post.find({postedBy: { $in : req.profile.following } })
  .populate('comments', 'text created')
  .populate('comments.postedBy', '_id username')
  .populate('postedBy', '_id username')
  .sort('-created')
  .exec((err, posts) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(posts)
  })
})

router.get('/posts/feed/by/:userId',authControl.confirmSignIn,function(req, res, next){
    Post.find({postedBy: req.profile._id})
  .populate('comments', 'text created')
  .populate('comments.postedBy', '_id username')
  .populate('postedBy', '_id username')
  .sort('-created')
  .exec((err, posts) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(posts)
  })
})
router.param('userId', function(req,res,next,userId){
    User.findById(userId)
    .populate('following', '_id username')
    .populate('followers', '_id username')
    .exec((err, user) => {
    if (err || !user) return res.status('400').json({
      error: "User not found"
    })
    req.profile = user
    next()
  })
  })

router.post('/posts/feed/new/:userId',authControl.confirmSignIn,function(req, res, next){
    let form = new formidable.IncomingForm()
    form.keepExtensions = true
    form.parse(req, (err, fields, files) => {
    if (err) {
      return res.status(400).json({
        error: "Post could not be uploaded"
      })
    }
    let post = new Post(fields)
    post.postedBy= req.profile
    if(files.photo){
      post.photo.data = fs.readFileSync(files.photo.path)
      post.photo.contentType = files.photo.type
    }
    post.save((err, result) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }
      res.json(result)
    })
  })
  })
  router.get('/posts/feed/photo/:postId',function(req, res, next){
    res.set("Content-Type", req.post.photo.contentType)
    return res.send(req.post.photo.data)
  })
  router.delete('/posts/:postId',authControl.confirmSignIn, postControl.isPoster,function(req,res,next){
    let post = req.post
    post.remove((err, deletedPost) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }
      res.json(deletedPost)
    })
  })
  router.put('/posts/like',authControl.confirmSignIn, function(req,res,next){
    console.log(req);
    Post.findByIdAndUpdate(req.body.postId,
      {$push: {likes: req.body.userId}}, {new: true})
       .exec((err, result) => {
         if (err) {
           return res.status(400).json({
             error: errorHandler.getErrorMessage(err)
           })
         }
         res.json(result)
       })
  })
  router.put('/posts/unlike',authControl.confirmSignIn, function(req,res,next){
    Post.findByIdAndUpdate(req.body.postId, {$pull: {likes: req.body.userId}}, {new: true})
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        })
      }
      res.json(result)
    })
  })
  router.put('/posts/comment',authControl.confirmSignIn, function(req,res,next){
    let comment = req.body.comment
    console.log(comment)
  comment.postedBy = req.body.userId
  Post.findByIdAndUpdate(req.body.postId,
 {$push: {comments: comment}}, {new: true})
  .populate('comments.postedBy', '_id username')
  .populate('postedBy', '_id username')
  .exec((err, result) => {
    if (err) {
      return res.status(400).json({
        error: err
      })
    }
    res.json(result)
  })
  })
  router.put('/posts/removecomment',authControl.confirmSignIn, function(req,res,next){
    let comment = req.body.comment
    Post.findByIdAndUpdate(req.body.postId, {$pull: {comments: {_id: comment._id}}}, {new: true})
    .populate('comments.postedBy', '_id username')
    .populate('postedBy', '_id username')
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: err
        })
      }
      res.json(result)
    })
  })
  router.param('postId', function(req, res, next,postId){
    Post.findById(postId).populate('postedBy', '_id username').exec((err, post) => {
        if (err || !post)
          return res.status('400').json({
            error: "Post not found"
          })
        req.post = post
        next()
      })
  })
  
  module.exports = router;