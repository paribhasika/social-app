const express = require('express');
const router = express.Router();
const config = require('../../database/db');
const User = require('../../models/user');
const jwt = require('jsonwebtoken');



// GET for  logout
router.get('/logout', function (req, res, next) {
    if (req.session) {
      // delete session object
      req.session.destroy(function (err) {
        if (err) {
          return next(err);
        } else {
          return res.redirect('/');
        }
      });
    }
  });

  router.post('/signin',function(req,res,next){
    if (req.body.email && req.body.password) {
        User.authenticate(req.body.email, req.body.password, function (error, user) {
          if (error || !user) {
            var err = new Error('Wrong email or password.');
            err.status = 401;
            return next(err);
          } else {
          //  req.session.userId = user._id;
            const token = jwt.sign({
                _id: user._id
              }, config.jwtSecret)
            return res.json({
                token,
                user: {userId:user._id, name: user.username, email: user.email}
              })
          }
        });
      }
  })

  router.post('/signup',function(req,res,next){
    // if (req.body.password !== req.body.passwordConf) {
    //     var err = new Error('Passwords do not match.');
    //     err.status = 400;
    //     res.send("passwords dont match");
    //     return next(err);
    //   }
    
      if (req.body.email &&
        req.body.username &&
        req.body.password ) {
    
        var userData = {
          email: req.body.email,
          username: req.body.username,
          password: req.body.password,
        }
    
        User.create(userData, function (error, user) {
          if (error) {
            return next(error);
          } else {
            const token = jwt.sign({
                _id: user._id
              }, config.jwtSecret)
            return res.json({
                token,
                user: {userId:user._id, name: user.username, email: user.email}
              })
          }
        });
    
      }  else {
        var err = new Error('All fields required.');
        err.status = 400;
        return next(err);
      }
  })

  module.exports = router;