const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const authControl=require('../../controllers/auth');
const followerControl=require('../../controllers/follower');
const userControl=require('../../controllers/user');


// @route   POST /
// @desc    
// @access
router.post('/users', function (req, res, next) {
  // confirm that user typed same password twice
  if (req.body.password !== req.body.passwordConf) {
    var err = new Error('Passwords do not match.');
    err.status = 400;
    res.send("passwords dont match");
    return next(err);
  }

  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {

    var userData = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
    }

    User.create(userData, function (error, user) {
      if (error) {
        return next(error);
      } else {
        res.json({created:'true'});
       // return res.redirect('/profile');
      }
    });

  }  else {
   var err = new Error('All fields required.');
    err.status = 400;
    return next(err);
  }
})

// GET route after registering
router.get('/users', function (req, res, next) {
  User.find((err, users) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    res.json(users)
  }).select('username email ')
});


router.put('/users/follow',followerControl.addFollowing,followerControl.addFollower);

router.put('/users/unfollow',authControl.confirmSignIn,followerControl.removeFollowing,followerControl.removeFollower);
router.get('/users/findpeople/:userId',authControl.confirmSignIn, followerControl.findPeople)
router.get('/users/:userId',authControl.confirmSignIn, function (req, res, next) {
  return res.json(req.profile)
});
router.put('/users/:userId',authControl.confirmSignIn,authControl.hasAccess, function (req, res, next) {
   User.findById(req.userId)
    .exec(function (error, user) {
      if (error) {
        return next(error);
      } else {
        if (user === null) {
          var err = new Error('Not authorized! Go back!');
          err.status = 400;
          return next(err);
        } else {
          return res.send('<h1>Name: </h1>' + user.username + '<h2>Mail: </h2>' + user.email + '<br><a type="button" href="/logout">Logout</a>')
        }
      }
    });
});


router.delete('/users/:userId',function(req,res,next){
  authControl.confirmSignIn(req,res,next);
  authControl.hasAccess(req,res,next);
  let user = req.profile
  user.remove((err, deletedUser) => {
    if (err) {
      return res.status(400).json({
        error: errorHandler.getErrorMessage(err)
      })
    }
    //deletedUser.hashed_password = undefined
    //deletedUser.salt = undefined
    res.json(deletedUser)
  })
});
router.param('userId',function(req,res,next,userId){
  User.findById(userId)
  .populate('following', '_id username')
  .populate('followers', '_id username')
  .exec((err, user) => {
    
  if (err || !user) return res.status('400').json({
    error: "User not found"
  })
  
  req.profile = user
  next()
})
});



module.exports = router;


/*router.route('/api/users')
  .get(userCtrl.list)
  .post(userCtrl.create)

router.route('/api/users/:userId')
  .get(userCtrl.read)
  .put(userCtrl.update)
  .delete(userCtrl.remove)

router.param('userId', userCtrl.userByID)*/


// router.route('/users/follow')
//   .put(authCtrl.requireSignin, userCtrl.addFollowing, userCtrl.addFollower)
// router.route('/users/unfollow')
//   .put(authCtrl.requireSignin, userCtrl.removeFollowing, userCtrl.removeFollower)