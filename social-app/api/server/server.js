const express = require('express');
const mongoose = require('mongoose');
const config = require('../database/db');
const bodyParser = require('body-parser');
const user = require('./routes/user')
const auth = require('./routes/auth')
const post = require('./routes/posts')
const cors = require('cors');
const app = express();
const PORT = 8081;

app.use(bodyParser.json());
app.use(cors())

mongoose
.connect(config.DB,{useNewUrlParser: true})
.then(()=>console.log('MongoDB connected..'))
.catch(err=>console.log(err)); 
mongoose.set('debug', true);
//Using Routes
app.use('/',user);
app.use('/auth',auth);
app.use('/', post);

app.listen(PORT,()=>console.log('Server started on port '+ PORT));