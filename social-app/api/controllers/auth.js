
const expressJwt = require('express-jwt');
const config = require('../database/db');
const confirmSignIn = expressJwt({
    secret: config.jwtSecret,
    userProperty: 'auth'
  })

  const hasAccess = (req, res, next) => {
    const authorized = req.profile && req.auth && req.profile._id == 
    req.auth._id
    if (!(authorized)) {
      return res.status('403').json({
        error: "Not authorized"
      })
    }
    next()
  }

  module.exports={confirmSignIn,hasAccess};