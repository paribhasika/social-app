const Post = require('../models/post-model');

const isPoster = (req, res, next) => {
    let isPosterAuth = req.post && req.auth &&
    req.post.postedBy._id == req.auth._id
    if(!isPosterAuth){
      return res.status('403').json({
        error: "User is not authorized"
      })
    }
    next()
}

module.exports={isPoster}