const User =require('../models/user');


const addFollowing = (req, res, next) => {
  
    User.findByIdAndUpdate(req.body.userId, {$push: {following: req.body.followId}}, (err, result) => {
      if (err) {
        return res.status(400).json({
          error: 'Here'
        })
      }
      next()
    })
  }
  
  const addFollower = (req, res) => {
    User.findByIdAndUpdate(req.body.followId, {$push: {followers: req.body.userId}}, {new: true})
    .populate('following', '_id username')
    .populate('followers', '_id username')
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: 'ERROR'
        })
      }
      result.hashed_password = undefined
      result.salt = undefined
      res.json(result)
    })
  }
  
  const removeFollowing = (req, res, next) => {
    User.findByIdAndUpdate(req.body.userId, {$pull: {following: req.body.unfollowId}}, (err, result) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }
      next()
    })
  }
  const removeFollower = (req, res) => {
    User.findByIdAndUpdate(req.body.unfollowId, {$pull: {followers: req.body.userId}}, {new: true})
    .populate('following', '_id username')
    .populate('followers', '_id username')
    .exec((err, result) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }
      result.hashed_password = undefined
      result.salt = undefined
      res.json(result)
    })
  }
  const findPeople = (req, res) => {
    let following = req.profile.following
    following.push(req.profile._id)
    User.find({ _id: { $nin : following } }, (err, users) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler.getErrorMessage(err)
        })
      }
      
    }).select('username email').exec((err,result)=>{
        if (err) {
            return res.status(400).json({
              error:err
            })
          }
         res.send(result)
    })
  }
  
  module.exports={
    addFollowing,
    addFollower,
    removeFollowing,
    removeFollower,
    findPeople}