const User =require('../models/user');

const userId = (req, res, next, userId) => {
    User.findById(userId)
      .populate('following', '_id username')
      .populate('followers', '_id username')
      .exec((err, user) => {
      if (err || !user) return res.status('400').json({
        error: "User not found"
      })
      req.profile = user
      next()
    })
  }
  
  module.exports=userId;